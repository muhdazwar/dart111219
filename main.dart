int add(a,b)
{
  return a + b;
}

int sub(int x, int y) => x-y;

double addDouble(int a, double b)
{
  return a + b;
}

int multiply(a, b)
{
  return a * b;
}

exec(Function func, x, y)
{
  // return func(x,y);
  return add(x,y);
}

Function addition;

void main(){
  
  addition = add;

  int x = 6;
  int y = 7;
  double z = 13.12;

  // print(add(x,y));
  // print(addition(x,y));

  // print(sub(x, y));

  // print(addDouble(x,z));

  // print(multiply(x,y));
  // print(exec(multiply,x,y));

  //anonymous function
  (a, b) {
    print("Hello, result = ${a + b }");
  }(10, 20);

}