void main(){
  
  //print("HelloWorld");
  //print("From Dart World");

  //var x = -10;
  //print(x.abs());
  
  // numbers - (int, float)
  // String - " ",
  // bool - true or false
  // List - (aka arrays) List<int) ___

  int y = 10;
  double z = 12.9;
  String s = "${y+z}";
  bool b = true;
  List l = [1,2,3];
  List<String> ls = ["1", "2", "3"];

  // print(y);
  // print(z);
  // print(s);
  // print(b);
  // print(l);
  // print(ls);

  // print("" + "${y}" + "\n" + "${z}" + "\n" + "${s}" + "\n" 
  // + "${b}" + "\n" + "${l[0]}" + "\n" + "${ls[2]}" );

  
  // Map - hasmaps
  // runes - unicode characeter sets
  // symbols - #symbol

  Map <String, int> maps = {
    'A': 10,
    'B': 20,
    'C': 30,
  };

  // print(maps['A']); 

  Map map = {
     12: 10,
    'B': 20,
    'C': 30,
  };

  // print(map['12']); // return null because of string
  // print(map[12]); // return value 10

  

}